var irc = require('irc');
var amqp = require('amqplib/callback_api');


var amqp_ch;
var amqp_conn;
var irc_client;
var proxies = {}; // mapa de proxys

// Conexão com o servidor AMQP
amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
        amqp_conn = conn;
        amqp_ch = ch;

        inicializar();
    });
});

function inicializar () {

    receberDoCliente("registro_conexao", function (msg) {
        var id       = msg.id;
        var servidor = msg.servidor;
        var nick     = msg.nick;
        var canal    = msg.canal;

        irc_client = new irc.Client(
            servidor, 
            nick,
            {channels: [canal]}
        );

        irc_client.addListener('message#', function (nick, canal, mensagem) {
            console.log(nick + ' => ' + canal + ': ' + mensagem);

            enviarParaCliente(id, {
                "timestamp": Date.now(), 
                "comando": "message#",
                "nick": nick,
                "canal": canal,
                "mensagem": mensagem
            });
        });
        
        irc_client.addListener('join', function (canal, nick) {
            console.log(canal + ' --> ' + nick + ' entrou no canal.');

            enviarParaCliente(id, {
                "timestamp": Date.now(), 
                "comando": "join",
                "nick": nick,
                "canal": canal
            });
        });
        
        irc_client.addListener('part', function (canal, nick, razao) {
            if(razao) {
                console.log(canal + ' --> ' + nick + ' saiu do canal: ' + razao);
                
                enviarParaCliente(id, {
                    "timestamp": Date.now(),
                    "comando": "part",
                    "nick": nick,
                    "canal": canal,
                    "mensagem": razao
                });
            } else {
                console.log(canal + ' --> ' + nick + ' saiu do canal.');
                
                enviarParaCliente(id, {
                    "timestamp": Date.now(),
                    "comando": "part",
                    "nick": nick,
                    "canal": canal
                });
            }
        });
        
        irc_client.addListener('names', function(canal, nicks) {
            console.log(canal + ' -> evento "names" disparado');
            
            enviarParaCliente(id, {
                "comando": "names",
                "nicks": nicks,
                "canal": canal
            });
        });
        
        irc_client.addListener('motd', function(message) {
		    console.log('motd: ', message);
		    enviarParaCliente(id, {
                "comando": "motd",
                "msg": message
            });
        });

        
        

        irc_client.addListener('error', function(mensagem) {
            console.log('error: ', mensagem);
        });

        proxies[id] = irc_client;
    });

    receberDoCliente("gravar_mensagem", function (msg) {
        var i = 0;
        var palavras = msg.msg.split(' ');
        var tamPalavras = palavras.length;

        console.log(msg);

        if(palavras[0] === "/msg") {
            concatMsg = "";
            for(i = 2; i < tamPalavras; i++) {
                concatMsg = concatMsg + palavras[i] + " ";
            }
            proxies[msg.id].ctcp(palavras[1], "privmsg", concatMsg);
        }
        else if(palavras[0] === "/join") {
            if((palavras[1] !== "") && (palavras[1] !== undefined)) {
                proxies[msg.id].join(palavras[1]);
            }
        }
        else if(palavras[0] === "/part") {
            if((palavras[1] !== "") && (palavras[1] !== undefined)) {
                concatMsg = "";
                for(i = 2; i < tamPalavras; i++) {
                    concatMsg = concatMsg + palavras[i] + " ";
                }
                proxies[msg.id].part(palavras[1], concatMsg);
            }
        }
        
    

        
	


        else {
            proxies[msg.id].say(msg.canal, msg.msg);
        }
    });
}

function receberDoCliente (fila, callback) {
    amqp_ch.assertQueue(fila, {durable: false});

    console.log(" [irc] Waiting for messages in ", fila);

    amqp_ch.consume(fila, function(msg) {
        console.log(" [irc] Received %s", msg.content.toString());
        callback(JSON.parse(msg.content.toString()));
    }, {noAck: true});
}

function enviarParaCliente (id, msg) {
    msg = new Buffer(JSON.stringify(msg));

    amqp_ch.assertQueue("user_"+id, {durable: false});
    amqp_ch.sendToQueue("user_"+id, msg);
    console.log(" [irc] Sent %s", msg);
}

