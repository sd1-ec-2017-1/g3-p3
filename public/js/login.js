/*
    Exibe os erros listados em "mensagemErro".
    "mensagemErro": vetor de strings, onde cada elemento corresponde a uma mensagem de erro distinta.
*/
function exibirErro(mensagemErro) {
    var divErro = document.getElementById('erro');
    var i = 0;
    var itemErro;  // Recebe um elemento recém-criado do tipo <li>
    var listaErro = document.getElementById('listaErro');  // Elemento do tipo <ul>
    var qtMensagens = mensagemErro.length;  // Quantidade de mensagens de erro.
    
    // Se já houver uma lista de erros sendo exibida, vamos limpá-la
    while (listaErro.lastChild) {
        listaErro.removeChild(listaErro.lastChild);
    }
    
    // Criando a nova lista de erros
    for (i = 0; i < qtMensagens; i++) {
        itemErro = document.createElement('li');
        itemErro.innerHTML = mensagemErro[i];
        listaErro.appendChild(itemErro);
    }
    
    divErro.style.display = "block";  // Torna a lista visível
    divErro.style.height = 50 + listaErro.offsetHeight;  // Redimensiona a altura do div 'erro' para que toda a lista de erros seja exibida
    document.getElementById('container').style.top = 30 + listaErro.offsetHeight;  // Desloca, para baixo, o formulário de login, para que não seja sobreposto pelo div 'erro'
}


/*
    Verifica se os dados digitados pelo usuário na tela de login são válidos
    Retorna true se os dados forem válidos e false se houver algo errado
*/
function validarDados() {
    var objRetorno = {dadosValidos:true, mensagemErro:[]};  // Objeto para armazenar a validade dos dados e mensagens de erro, caso existam
    
    document.getElementById('erro').style.display = "none";  // Se a lista de erros estiver visível, torna-a invisível
    
    objRetorno = validarNick(document.forms["formLogin"]["nick"].value, objRetorno);  // Verifica nick fornecido no login
    objRetorno = validarCanal(document.forms["formLogin"]["canal"].value, objRetorno);  // Verifica canal fornecido no login
    objRetorno = validarServidor(document.forms["formLogin"]["servidor"].value, objRetorno);  // Verifica servidor fornecido no login
    
    if (!objRetorno.dadosValidos) {  // Se houver algum erro nos dados de login...
        exibirErro(objRetorno.mensagemErro);
    }
    
    return objRetorno.dadosValidos;
}

