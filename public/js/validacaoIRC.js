/*
    Verifica se o nome do canal é válido, ou seja, se ele obedece a regra para nomes de canais estabelecida no RFC 2812 seção 2.3.1, com exceção ao nome do canal começar com '!', pois nomes de canal iniciados com '!' não foram aceitos em testes realizados no freenode.
    "canal": string com o nome do canal a ser verificado.
    "objRetorno": é um objeto composto por: uma variável booleana ("dadosValidos") que recebe false caso haja algum erro no nome do canal; e um vetor de strings (mensagemErro) que recebe a/as mensagem/mensagens do/dos erro/erros que existir/existirem. Este objeto é retornado pela função.
*/
function validarCanal(canal, objRetorno) {    
    if ((canal !== "") && (canal !== undefined)) {
        var inicial = /^(?:\x23|\x26|\x2B)/;  // Expressão regular que utiliza o hexadecimal dos caracteres especiais: '#', '&', '+'
        var naoDeveTer = /(?:\x00|\x07|\x0A|\x0D|\x20|\x2C)/;  // Expressão regular que utiliza o hexadecimal de NUL, BELL, CR, LF, ' ', ','

        if (!(inicial.test(canal))) {  // Se o nome do canal NÃO começar com um dos 3 caracteres permitidos...
            objRetorno.mensagemErro.push("Nome do canal deve começar com: '#', '&' ou '+'.\n");
            objRetorno.dadosValidos = false;
        } else if (canal.length < 2) {  // Caso haja somente o caracter inicial
            objRetorno.mensagemErro.push("Nome do canal deve conter pelo menos mais um caracter!\n");
            objRetorno.dadosValidos = false;
        }
        
        if (naoDeveTer.test(canal)) {  // Verifica a presença de algum caracter inválido no nome do canal
            objRetorno.mensagemErro.push("Nome do canal contém caracter inválido!\n");
            objRetorno.dadosValidos = false;
        } else if (/::/.test(canal)) {
            objRetorno.mensagemErro.push("Nome do canal não pode conter substring '::' !\n");
            objRetorno.dadosValidos = false;
        }

    } else {
        objRetorno.mensagemErro.push("Canal não digitado!\n");
        objRetorno.dadosValidos = false;
    }
    
    return objRetorno;
}


/*
    Verifica se o nick é válido, ou seja, se ele obedece a regra para nicknames estabelecida no RFC 2812 seção 2.3.1.
    "nickname": string com o nick a ser verificado.
    "objRetorno": é um objeto composto por: uma variável booleana ("dadosValidos") que recebe false caso haja algum erro no nick; e um vetor de strings (mensagemErro) que recebe a/as mensagem/mensagens do/dos erro/erros que existir/existirem. Este objeto é retornado pela função.
*/
function validarNick(nickname, objRetorno) {
    if (nickname !== "") {
        var digito = /[0-9]/;  //Expressão regular para um dígito de 0 à 9
        var especial = /([\x5B-\x60]|[\x7B-\x7D])/;  //Expressão regular que busca em faixas hexadecimais pelos caracteres especiais: "[", "]", "\", "`", "_", "^", "{", "|", "}"
        var i = 1;
        var letra = /[A-Z]/i;  //Expressão regular para uma letra de A à Z (case insensitive)
        var nick = nickname.split("");  //Transforma cada char de nickname em um elemento de um vetor
        var tamanhoNick = nick.length;

        if (!(letra.test(nick[0]) || especial.test(nick[0]))) {  // Verifica o primeiro caracter (regra diferente da dos demais caracteres)
            objRetorno.mensagemErro.push("Nick não deve começar com '" + nick[0] + "'!\n");
            objRetorno.dadosValidos = false;
        }
        for (i = 1; i < tamanhoNick; i++) {  // Verifica os demais caracteres
            if (!(letra.test(nick[i]) || digito.test(nick[i]) || especial.test(nick[i]) || /\-/.test(nick[i]))) {
                objRetorno.mensagemErro.push("Nick não deve conter '" + nick[i] + "'!\n");
                objRetorno.dadosValidos = false;
            }
        }
    } else {
        objRetorno.mensagemErro.push("Nick não digitado!\n");
        objRetorno.dadosValidos = false;
    }
    
    return objRetorno;
}


/*
    Verifica se o nome do servidor é válido de acordo com a regra para nomes de servidores estabelecida no RFC 2812 seção 2.3.1. NÃO verifica se o servidor realmente existe!
    "servidor": string com o nome de servidor a ser verificado.
    "objRetorno": é um objeto composto por: uma variável booleana ("dadosValidos") que recebe false caso haja algum erro no nome do servidor; e um vetor de strings (mensagemErro) que recebe a/as mensagem/mensagens do/dos erro/erros que existir/existirem. Este objeto é retornado pela função.
*/
function validarServidor(servidor, objRetorno) {
    if (servidor !== "") {
        var digito = /[0-9]/;
        var especial = /\-|\./;  // Expressão regular para o caracter '-' ou '.'
        var i = 1;
        var letra = /[A-Z]/i;  // Expressão regular para letras case insensitive
        var posUltimoCaracter = servidor.length - 1;
        var vetorServidor = servidor.split("");  //Transforma cada char de "servidor" em um elemento de um vetor
        
        if (!(letra.test(vetorServidor[0]) || digito.test(vetorServidor[0]))) {  // Verifica o primeiro caracter (regra diferente da dos demais caracteres)
            objRetorno.mensagemErro.push("Servidor não deve começar com '" + vetorServidor[0] + "'!\n");
            objRetorno.dadosValidos = false;
        }
        if (!(letra.test(vetorServidor[posUltimoCaracter]) || digito.test(vetorServidor[posUltimoCaracter]))) {  // Verifica o último caracter (mesma regra do primeiro)
            objRetorno.mensagemErro.push("Servidor não deve terminar com '" + vetorServidor[posUltimoCaracter] + "'!\n");
            objRetorno.dadosValidos = false;
        }
        for (i = 1; i < posUltimoCaracter; i++) {  // Verifica os demais caracteres
            if (!(letra.test(vetorServidor[i]) || digito.test(vetorServidor[i]) || especial.test(vetorServidor[i]))) {
                objRetorno.mensagemErro.push("Servidor não deve conter '" + vetorServidor[i] + "'!\n");
                objRetorno.dadosValidos = false;
            }
        }
        
        if (/(?:\-|\.){2,}/.test(servidor)) {
            objRetorno.mensagemErro.push("Servidor não deve conter as substring:  '..',  '.-',  '--',  '-.'  !\n");
            objRetorno.dadosValidos = false;
        }
    } else {
        objRetorno.mensagemErro.push("Servidor não digitado!\n");
        objRetorno.dadosValidos = false;
    }
    
    return objRetorno;
}

