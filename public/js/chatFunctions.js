var canais = {};
var canalAtivo = "";
var canalId = 0;
var myId;
var myNick = "";
var myServer = "";
var novoTimestamp = 0;

$(document).ready(function () {

    myId = Cookies.get("id");
    myNick = Cookies.get("nick");
    myServer = Cookies.get("servidor");

    criarCanal(Cookies.get("canal"), true);
    adicionarMensagem(canalAtivo, canalAtivo + ' => ' + myNick + ' entrou no canal.');

    carregarMensagens(novoTimestamp);

    $('#butao').click(function() {enviarMensagem();});

    $('#mensagem').keypress(function(event) {
        if(event.which === 13){
            event.preventDefault();
            enviarMensagem();
        }  
    });
});

/*
  Adiciona a mensagem <msg> no mural do canal <canal>.
*/
function adicionarMensagem(canal, msg) {
    var mural = $('#mural'.concat(canais[canal]));
    mural.append($('<div></div>').text(msg));
    mural.scrollTop(mural.prop('scrollHeight'));
}

function carregarMensagens(timestamp) {
    $.get("obter_mensagem/"+timestamp, {id: myId}, function(mensagens, status) {
        if(status === "success") {
            if(mensagens) {
                var i = 0;
                var ultimaMsg = mensagens.length - 1;
                
                for(i = ultimaMsg; i >= 0; i--) {
                    novoTimestamp = mensagens[i].timestamp;
                    processarMensagem(mensagens[i]);
                }
            }
        } else {
            alert("Erro: " + status);
        }
    });
    
    var t = setTimeout(function() {carregarMensagens(novoTimestamp);}, 1000);
}

function criarCanal(canal, usaNicks) {
    var cId = canalId.toString();
    canais[canal] = cId;

    var novo_botaoCanal = $('<div class="botaoCanal"></div>');
    novo_botaoCanal.append($('<span id="botao'+cId+'"></span>').text(canal));
    novo_botaoCanal.click(function(){exibirCanal($(this));});
    $('#salas').append(novo_botaoCanal);

    var novo_mural = $('<div class="mural" id="mural'+cId+'"></div>');
    $('#chat').append(novo_mural);

    if(usaNicks) {
        var novo_nicksDoCanal = $('<div class="nicksNoCanal" id="nicks'+cId+'"></div>');
        novo_nicksDoCanal.append($('<h3></h3>').text("Nicks no canal:"));
        $('#chat').append(novo_nicksDoCanal);
    }

    canalId++;

    exibirCanal($('#botao'.concat(cId)));
}

function deletarCanal(canal) {
    var cId = canais[canal];

    if(canal === canalAtivo) {
        var spans = document.getElementsByTagName("SPAN");
        
        if(spans.length > 1) {
            if(spans[0].innerHTML !== canal) {
                exibirCanal($('#'.concat(spans[0].id)));
            } else {
                exibirCanal($('#'.concat(spans[1].id)));
            }
        }
    }

    $('#botao'.concat(cId)).parent().remove();
    $('#mural'.concat(cId)).remove();
    $('#nicks'.concat(cId)).remove();

    delete canais[canal];
}

function enviarMensagem() {
    var msg = '{"id":' + myId + ',' +
                '"canal":"' + canalAtivo + '",' + 
                '"msg":"' + $('#mensagem').val() + '"}';

    $.ajax({
        type: "post",
        url: "/gravar_mensagem",
        data: msg,
        success: function(data, status) {
            if (status == "success") {

            } else {
                alert("erro:"+status);
                return;
            }
        },
        contentType: "application/json",
        dataType: "json"
    });

    preProcessarMensagem($('#mensagem').val());
    $('#mensagem').val('');    
}

function exibirCanal(elemento) {
    var cId = "";

    // Escondendo o canal que até então estava ativo
    if(canalAtivo !== "") {
        cId = canais[canalAtivo];
        $('#mural'.concat(cId)).toggle();

        if($('#nicks'.concat(cId))) {
            $('#nicks'.concat(cId)).toggle();
        }
    }

    // Tornando visível o canal que se deseja exibir
    cId = canais[elemento.text()];
    $('#mural'.concat(cId)).toggle();

    if($('#nicks'.concat(cId))) {
        $('#nicks'.concat(cId)).toggle();
    }

    canalAtivo = elemento.text();
}

function preProcessarMensagem(mensagem) {
    var objValidacao = {dadosValidos:true, mensagemErro:[]};  // Objeto para armazenar a validade dos dados e mensagens de erro, caso existam
    var palavras = mensagem.split(' ');

    if(palavras[0] === "/join") {
        var canal = "";
        var i = 0;
        var vetorCanais = palavras[1].split(',');
        var tamVetor = vetorCanais.length;
        
        for(i = 0; i < tamVetor; i++) {
            canal = vetorCanais[i];
            
            if(!canais[canal]) {
                objValidacao = validarCanal(canal, objValidacao);
                if(objValidacao.dadosValidos) {
                    criarCanal(canal, true);
                    adicionarMensagem(canal, canal + " => " + myNick + " entrou no canal.");
                } else {
                    alert("Erro (" + canal + "): \n" + objValidacao.mensagemErro);
                }
            } else {
                exibirCanal($('#botao'.concat(canais[canal])));
            }
            
            objValidacao.dadosValidos = true;
            objValidacao.mensagemErro = [];
        }
    } else if(palavras[0] === "/part") {
        var canal = "";
        var i = 0;
        var vetorCanais = palavras[1].split(',');
        var tamVetor = vetorCanais.length;
        
        for(i = 0; i < tamVetor; i++) {
            canal = vetorCanais[i];
            
            if(canais[canal]) {
                deletarCanal(canal);
            }
        }
    } else {
        adicionarMensagem(canalAtivo, "[" + timestamp_para_data(Date.now()) + " - " + myNick + "]: " + mensagem);
    }

}

function processarMensagem(msg) {
    switch (msg.comando) {
    case "message#":
        adicionarMensagem(msg.canal, "[" + timestamp_para_data(msg.timestamp) + " - " + msg.nick + "]: " + msg.mensagem);
        break;
    case "join":
        if(msg.nick !== myNick) {
            $('#nicks'.concat(canais[msg.canal])).append($('<div></div>').text(msg.nick));
            adicionarMensagem(msg.canal, msg.canal + " => " + msg.nick + " entrou no canal.");
        }
        break;
    case "part":
        if(msg.nick !== myNick) {
            $('#nicks'.concat(canais[msg.canal])).children("div").filter(":contains("+ msg.nick +")").remove();
            if(msg.mensagem) {
                adicionarMensagem(msg.canal, msg.canal + " => " + msg.nick + " saiu do canal: " + msg.mensagem);
            } else {
                adicionarMensagem(msg.canal, msg.canal + " => " + msg.nick + " saiu do canal.");
            }
        }
        break;
    case "names":
        for(var nick in msg.nicks) {
            $('#nicks'.concat(canais[msg.canal])).append($('<div></div>').text(nick));
        }
        break;
    case "motd":
               var i = 0;
        var motd = msg.msg.split('\n');
        var tamanhoMotd = motd.length;

        while(canalAtivo === "");

        for(i = 0; i < tamanhoMotd; i++) {
            adicionarMensagem(canalAtivo, motd[i]);
        }

        break;
    default:
        console.log(msg);
    }
}

/*
  Transforma timestamp em formato HH:MM:SS
*/
function timestamp_para_data(timestamp) {
	var data = new Date(timestamp);
	var horas = data.getHours();
	var sHoras = horas < 10 ? "0"+horas : ""+horas;
	var minutos = data.getMinutes();
	var sMinutos = minutos < 10 ? "0"+minutos : ""+minutos;
	var segundos = data.getSeconds();
	var sSegundos = segundos < 10 ? "0"+segundos : ""+segundos;
	return sHoras + ":" + sMinutos + ":" + sSegundos;
}
